#!/bin/bash
#
#  Copyright (C) 2017 Codethink Limited
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Tristan Van Berkom <tristan.vanberkom@codethink.co.uk>

#####################################################
#          Usage and command line parsing           #
#####################################################
function usage () {
    echo "Usage: "
    echo "  autoconvert.sh [OPTIONS]"
    echo
    echo "Automatically converts the latest GNOME modulesets to BuildStream format"
    echo
    echo "Options:"
    echo
    echo "  -h --help                    Display this help message and exit"
    echo "  -s --source      <remote>    Remote name to pull base from"
    echo "  -d --dest        <remote>    Remote name to pull current branches to"
    echo "  -m --modulesets  <directory> The BuildStream modulesets git repository"
    echo "  -j --jhbuild2bst <directory> The jhbuild2bst git repository"
    echo "  --pushmaster                 Push to the master branch of dest, instead of current"
    echo "  --track                      Run bst track afterwards, and push to the tracked branch of dest"
    echo "  --date           <date>      Optional date stamp to use"
    echo "  "
    exit 1;
}

arg_source_remote="origin"
arg_dest_remote="origin"
arg_modulesets=
arg_jhbuild2bst=
arg_datestamp=$(date +%Y-%m-%d-%H%M%S)
arg_pushmaster=false
arg_track=false

while : ; do
    case "$1" in 
	-h|--help)
	    usage;
	    shift ;;

	-s|--source)
	    arg_source_remote=${2};
	    shift 2 ;;

	-d|--dest)
	    arg_dest_remote=${2};
	    shift 2 ;;

	-m|--modulesets)
	    arg_modulesets=${2};
	    shift 2 ;;

	-j|--jhbuild2bst)
	    arg_jhbuild2bst=${2};
	    shift 2 ;;

	--date)
	    arg_datestamp=${2};
	    shift 2 ;;

	--pushmaster)
	    arg_pushmaster=true;
	    shift ;;

	--track)
	    arg_track=true;
	    shift ;;

	*)
	    if [ ! -z "${1}" ]; then
		echo "Unrecognized argument ${1}"
		usage
	    fi
	    break ;;
    esac
done

if [ -z "${arg_modulesets}" ]; then
    echo "Must specify the modulesets git repository"
    echo
    usage
fi

if [ -z "${arg_jhbuild2bst}" ]; then
    echo "Must specify the jhbuild2bst git repository"
    echo
    usage
fi

arg_modulesets="$(cd ${arg_modulesets} && pwd)"
arg_jhbuild2bst="$(cd ${arg_jhbuild2bst} && pwd)"

######################################################################
#                              Functions                             #
######################################################################

# Updates the local jhbuild2bst installation to the latest
#
function updateJhbuild2Bst() {
    cd "${arg_jhbuild2bst}"
    git pull --rebase
    make install
}

# Prepares the workspace for running a conversion, by ensuring
# we are on the "current" branch and that we have a clean state
# of the latest master.
#
# $1 - The repository checkout
# $2 - Remote name
function prepareWorkspace() {
    local repo=${1}
    local remote=${2}

    cd "${repo}"

    # Ensure we're on the 'current' branch
    git checkout current

    # Get latest
    git fetch "${remote}"

    # Remove all files in the working tree
    rm -rf *

    # Make the working tree exactly current master
    git checkout "${remote}/master" -- .
}

# Commit the results of something in a checkout
#
# $1 - The repository checkout
# $2 - Remote name
# $3 - Remote branch to push to
# $4 - The commit message
#
function commitWorkspace() {
    local repo=${1}
    local remote=${2}
    local branch=${3}
    local message=${4}

    cd "${repo}"
    git add .
    git commit -a -m "${message}"
    git push "${remote}" "current:${branch}"
}

# Run jhbuild2bst
#
# $1 - The modulesets repository (with base skeleton project)
# $2 - List of jhbuild targets to pass into the conversion
#
function runConversion() {
    local repo=${1}
    local targets=${2}

    # Targets is a word list, it must remain unquoted so that it
    # will be split into multiple arguments
    jhbuild bst --directory "${repo}" ${targets}
}

# Run bst track for all arches
#
# $1 - The modulesets repository (with base skeleton project)
#
function bstTrack() {
    local repo=${1}
    local targets="core-deps core apps"

    cd "${repo}"

    # HACK to work around ostree not allowing cloning through
    # the https:// URI, when the URI is being hosted on the
    # same machine we're running on
    #
    sed -e 's|gnome7:repo|file:///home/builder/debootstrap-ostree/work/export/repo|g' \
	-i elements/base/base-system.bst

    # Track every target under every arch
    for arch in i386 x86_64 arm aarch64; do
	bst --option arch "${arch}" --on-error continue track --deps all "base.bst"
    done
    for target in ${targets}; do
	bst --option arch "${arch}" --on-error continue track --deps all "${target}.bst" --except "base.bst"
    done

    # Undo hack when we're done
    sed -e 's|file:///home/builder/debootstrap-ostree/work/export/repo|gnome7:repo|g' \
	-i elements/base/base-system.bst
}

######################################################################
#                                 Main                               #
######################################################################
jhbuild_targets="meta-gnome-extended-devel-platform meta-gnome-core meta-gnome-core-os-services meta-gnome-apps-tested"

# Update jhbuild2bst
updateJhbuild2Bst

# Run the conversion
prepareWorkspace "${arg_modulesets}" "${arg_source_remote}"
runConversion "${arg_modulesets}" "${jhbuild_targets}"

push_branch="current"
if $arg_track; then
    push_branch="tracked"    
    bstTrack "${arg_modulesets}"
elif $arg_pushmaster; then
    push_branch="master"
fi

commitWorkspace "${arg_modulesets}" "${arg_dest_remote}" "${push_branch}" "Automated conversion of jhbuild modulesets (${arg_datestamp})"
