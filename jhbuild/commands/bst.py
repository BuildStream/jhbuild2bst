# jhbuild - a tool to ease building collections of source packages
# Copyright (C) 2017  Codethink Limited
#
#   bst.py: convert jhbuild modulesets to a BuildStream project
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import os
import optparse
import re
import stat
import sys
import time
import errno
import string
from optparse import make_option
import logging

import jhbuild.moduleset
import jhbuild.frontends
from jhbuild.errors import UsageError, FatalError, CommandError
from jhbuild.commands import Command, BuildCommand, register_command

from jhbuild.config import parse_relative_time

try:
    from ruamel import yaml
except ImportError:
    print("Converting to BuildStream requires the ruamel.yaml library.")
    sys.exit(1)

class BstDumper(yaml.SafeDumper):
    keyorder = (
        # Project related
        'name',
        'aliases',
        'element-path',
        'options',

        # Toplevel symbols
        'kind',
        'description',
        'sources',
        'depends',
        'variables',
        'environment',
        'config',
        'public',

        # Sources
        'url',
        'track',
        'ref',

        # Commands
        'configure-commands',
        'build-commands',
        'install-commands',
    )

    @classmethod
    def _iter_in_global_order(cls, mapping):
        for key in cls.keyorder:
            if key in mapping:
                yield key, mapping[key]
        for key in sorted(mapping.iterkeys()):
            if key not in cls.keyorder:
                yield key, mapping[key]

    @classmethod
    def _represent_dict(cls, dumper, mapping):
        return dumper.represent_mapping('tag:yaml.org,2002:map',
                                        cls._iter_in_global_order(mapping))

    @classmethod
    def _represent_str(cls, dumper, orig_data):
        fallback_representer = yaml.representer.SafeRepresenter.represent_str
        try:
            data = unicode(orig_data, 'ascii')
            if data.count('\n') == 0:
                return fallback_representer(dumper, orig_data)
        except UnicodeDecodeError:
            try:
                data = unicode(orig_data, 'utf-8')
                if data.count('\n') == 0:
                    return fallback_representer(dumper, orig_data)
            except UnicodeDecodeError:
                return fallback_representer(dumper, orig_data)
        return dumper.represent_scalar(u'tag:yaml.org,2002:str',
                                       data, style='|')

    @classmethod
    def _represent_unicode(cls, dumper, data):
        if data.count('\n') == 0:
            return yaml.representer.SafeRepresenter.represent_unicode(dumper,
                                                                      data)
        return dumper.represent_scalar(u'tag:yaml.org,2002:str',
                                       data, style='|')

    def __init__(self, *args, **kwargs):
        yaml.SafeDumper.__init__(self, *args, **kwargs)
        self.add_representer(dict, self._represent_dict)
        self.add_representer(str, self._represent_str)
        self.add_representer(unicode, self._represent_unicode)


def bst_dump(node, filename):

    # Ensure the directory where we're going to write exists.
    directory = os.path.dirname(filename)
    try:
        os.makedirs(directory)
    except OSError as e:
        # It's ok if the directory already exists
        if e.errno != errno.EEXIST:
            raise

    with open(filename, 'w') as f:
        yaml.dump(node, f, Dumper=BstDumper, default_flow_style=False, line_break="\n")


# Helper function to make things less wordy
def get_dict(dictionary, name):
    if name not in dictionary:
        dictionary[name] = {}
    return dictionary[name]

# Add a submodule declaration to the first source of
# an element (used for some special casing)
def add_submodule(element, url, path):
    sources = element['sources']
    source = sources[0]
    modules = get_dict(source, 'submodules')
    module = get_dict(modules, path)
    module['url'] = url


# Normalize a repo name, this is in the case
# of release modulesets when urls are used as module names
def normalize_repo_name(name):
    valid_chars = string.digits + string.ascii_letters + '%_'

    def transl(x):
        return x if x in valid_chars else '_'

    return ''.join([transl(x) for x in name])


class cmd_bst(Command):
    doc = N_('Convert the modulesets to a bst project')

    name = 'bst'
    usage_args = N_('[ options ... ] [ modules ... ]')

    def __init__(self):
        Command.__init__(self, [
            make_option('-d', '--directory', metavar='PATH', dest='directory',
                        help=_('Directory to output the converted modulesets')),
            ])

    def run(self, config, options, args, help=None):
        config.set_from_cmdline_options(options)

        # Override prefix to a real prefix, this is needed because some
        # module types use it to create additional configure arguments.
        #
        config.prefix = '/usr'

        # Dont allow 'partial_build' there is no such thing as
        # a host system, so dont interrogate it.
        #
        # We probably want to instead define system dependencies
        # further up the stack so that we dont bother with building
        # a lot of this stuff.
        config.partial_build = False

        def element_stack_name(module):
            moduleset_name = str(module.moduleset_name)
            # Create a clean base directory name, by stripping away some
            # of the leading moduleset name and stripping away the trailing
            # version number
            if moduleset_name.startswith('gnome-suites-'):
                stack_name = moduleset_name[len('gnome-suites-'):]
            elif moduleset_name.startswith('gnome-'):
                stack_name = moduleset_name[len('gnome-'):]
            else:
                # This shouldnt happen really
                stack_name = moduleset_name

            return stack_name.rstrip(string.digits + '.-')

        def element_name(module):
            modulename = str(module.name)
            stack_name = element_stack_name(module)

            return os.path.join(stack_name, modulename + '.bst')

        def convert_project(config, module_list):

            print "Generating project.conf"

            project = {}
            project['name'] = 'gnome'
            project['element-path'] = 'elements'
            project['environment'] = {
                'LC_ALL': 'en_US.UTF-8',
                'PKG_CONFIG_PATH': '/usr/local/lib/pkgconfig:%{libdir}/pkgconfig',
                'LDFLAGS': '-L%{libdir}',
                'LD_LIBRARY_PATH': '%{libdir}',
                'PATH': '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin'
            }

            elements = get_dict(project, 'elements')

            if config.autogenargs:
                autotools = get_dict(elements, 'autotools')
                variables = get_dict(autotools, 'variables')
                variables['conf-extra'] = config.autogenargs + ' --disable-Werror'

            if config.cmakeargs:
                cmake = get_dict(elements, 'cmake')
                variables = get_dict(cmake, 'variables')
                variables['cmake-extra'] = config.cmakeargs

            if config.mesonargs:
                meson = get_dict(elements, 'meson')
                variables = get_dict(meson, 'variables')
                variables['meson-extra'] = config.mesonargs

            # Dont use --prefix arg with distutils, maybe we
            # need to remove it from distutils defaults, nobody
            # seems to be using this (both fedora spec files
            # and debian rules dont pass --prefix to setup.py)
            distutils = get_dict(elements, 'distutils')
            variables = get_dict(distutils, 'variables')
            variables['python-install'] = '%{python} setup.py install --root "%{install-root}"'
            variables['prefix'] = '/usr/local'

            # Resolve the aliases
            project['aliases'] = {
                normalize_repo_name(repo.name): repo.href
                for repo in [
                    module.branch.repository
                    for module in module_list
                    if module.branch is not None
                ]}

            # Add gnome7 codethink alias for downloading ostree repos
            project['aliases']['gnome7'] = 'http://gnome7.codethink.co.uk/'

            # Special case: Add an alias for libpinyin's data model
            project['aliases']['downloads.sourceforge.net'] = 'http://downloads.sourceforge.net/'

            # Give us some architecture options
            project['options'] = {
                'arch': {
                    'type': 'arch',
                    'description': 'Machine architecture',
                    'values': [
                        'arm',
                        'aarch64',
                        'i386',
                        'x86_64',
                    ]
                }
            }

            # Setup the artifact server for pull only, unfortunately
            # for now this is https only since we dont have a GPG signing
            # solution yet.
            project['artifacts'] = {
                'url': 'https://gnome7.codethink.co.uk/artifacts'
            }

            project_file = os.path.join(config.directory, 'project.conf')
            bst_dump(project, project_file)

        def convert_elements(config, module_set, module_list):

            # Collect the elements which go into each stack, we
            # generate one stack per moduleset file that we converted.
            stacks = {}

            for module in module_list:

                print "Generating {} (type: {}, from: {})".format(element_name(module),
                                                                  type(module).__name__,
                                                                  module.moduleset_name)
                element = module.convert_bst(config)

                # Accumulate elements in the stacks we generate
                stack_name = element_stack_name(module)
                if stack_name not in stacks:
                    stacks[stack_name] = [element_name(module)]
                else:
                    stacks[stack_name].append(element_name(module))

                # For now, just continue if the conversion is not implemented yet
                # for the given element type
                if not element:
                    continue

                if not 'depends' in element:
                    element['depends'] = []

                # Start by normalizing all dependency names to modules
                #
                dep_modules = [
                    module_set.get_module(d)
                    for d in element['depends']
                    if module_set.module_exists(d)
                ]

                # Take any dependency that we depend on which has 'after' dependencies,
                # and additionally depend on the 'after' of anything we depend on.
                #
                after_modules = []
                for dep in dep_modules:
                    if dep.after:
                        after_modules += [
                            module_set.get_module(after)
                            for after in dep.after
                            if module_set.module_exists(after)
                        ]
                dep_modules += after_modules

                # Filter out sysdeps, they dont count anymore
                dep_modules = [
                    dep for dep in dep_modules

                    # Check type name, since importing SystemModule here
                    # causes problems and the `bst` command no longer shows up
                    if type(dep).__name__ != 'SystemModule'
                ]

                # Rename them with bst filenames, and exclude sysdeps
                element['depends'] = sorted([
                    element_name(dep) for dep in dep_modules
                ])

                # Append the base dependency
                element['depends'].append('base.bst')

                #######################################
                #      Special cases up ahead !!      #
                #######################################

                # General special casing of configure-commands
                #
                if 'config' in element:
                    element_config = element['config']
                    if 'configure-commands' in element_config:
                        conf_cmds = element_config['configure-commands']

                        # Rewrite the configure commands without the systemunitdir overrides.
                        element_config['configure-commands'] = [
                            cmd

                            # Various system service modules specify --with-systemdsystemunitdir=no
                            # with the intention of avoiding installing service files into the users'
                            # prefix, the result is that service files get installed as 'no' in the
                            # current working directory of the build.
                            #
                            # For the buildstream builds, we rather actually install these, the
                            # default will be a pkg-config query of the actual system unit dir.
                            #
                            .replace('--with-systemdsystemunitdir=no', '')

                            # GDM has this to store logs in the jhbuild prefix, just remove that too
                            #
                            .replace('--with-log-dir=/usr/var/log/gdm', '')

                            # Stripping any trailing whitespace from all the commands causes better
                            # YAML output to be produced
                            #
                            .rstrip()
                            for cmd in conf_cmds
                        ]

                # Same as above, but for the conf-local variable
                if 'variables' in element:
                    element_vars = element['variables']
                    if 'conf-local' in element_vars:
                        element_vars['conf-local'] = element_vars['conf-local'] \
                            .replace('--with-systemdsystemunitdir=no', '') \
                            .replace('--with-log-dir=/usr/var/log/gdm', '') \
                            .rstrip()

                # The mm-common module wants to download something at build time
                # (which is a no-no, not allowed in buildstream) and this cant be easily
                # disabled, so make sure we have one of what it wants installed in the
                # base system and copy it into place first.
                #
                if module.name == 'mm-common':

                    # Copy the system installed libstdc++.tag into doctags so we dont
                    # have to download it (cause we cant anyway), may need to make this
                    # command more robust when building against a multitude of different
                    # base rootfs variations.
                    element_config = get_dict(element, 'config')
                    element_config['configure-commands'] = {
                        '(<)': [
                            'cp /usr/share/doc/*/libstdc++/user/libstdc++.tag doctags/'
                        ]
                    }

                    variables = get_dict(element, 'variables')
                    variables['conf-local'] = '--disable-network'

                # depending on the compiler, libgtop will fail if not given the -std=c99
                # option, as it has code which declares variables not at the beginning
                # of a block statement.
                #
                elif module.name == 'libgtop':
                    # Set CC in the environment
                    env = get_dict(element, 'environment')
                    env['CC'] = 'gcc -std=c99'

                # When using distutils to install py3cairo, the default ends
                # up being /usr/local but the generated pkg-config file thinks
                # that the prefix is /usr, we need to correct the installed
                # pkg-config file for this.
                #
                elif module.name == 'py3cairo':
                    element_config = get_dict(element, 'config')
                    element_config['install-commands'] = {
                        '(>)': [
                            "sed -i -e 's|/usr|%{prefix}|g' %{install-root}%{prefix}/lib/pkgconfig/py3cairo.pc"
                        ]
                    }

                # Manually migrate the jhbuild trigger for glib
                elif module.name == 'glib':
                    public = get_dict(element, 'public')
                    bst = get_dict(public, 'bst')
                    bst['integration-commands'] = [
                        'glib-compile-schemas %{prefix}/share/glib-2.0/schemas'
                    ]

                # Manually migrate the jhbuild trigger for gdk-pixbuf
                elif module.name == 'gdk-pixbuf':
                    public = get_dict(element, 'public')
                    bst = get_dict(public, 'bst')
                    bst['integration-commands'] = [
                        'gdk-pixbuf-query-loaders --update-cache'
                    ]

                # Manually migrate the jhbuild trigger for gtk+
                elif module.name == 'gtk+-3':
                    public = get_dict(element, 'public')
                    bst = get_dict(public, 'bst')
                    bst['integration-commands'] = [
                        'for dir in %{prefix}/share/icons/*; do\n' + \
                        '  if test -f $dir/index.theme; then\n' + \
                        '    gtk-update-icon-cache --quiet $dir\n' + \
                        '  fi\n' + \
                        'done',
                        'gtk-query-immodules-3.0 --update-cache'
                    ]

                # gedit has a submodule, we want to have it specified in the bst file
                # (also this one is weird, because it tries to clone from ../libgd)
                #
                # When building the gedit tarball, dont add the submodule
                #
                elif module.name == 'gedit' and \
                     type(module.branch).__name__ != 'TarballBranch':
                    add_submodule(element, 'git_gnome_org:libgd', 'libgd')

                # Add the data model source for libpinyin, otherwise it tries to access the network
                elif module.name == 'libpinyin':
                    sources = element['sources']
                    sources.append({
                        'kind': 'tar',
                        'url': 'downloads.sourceforge.net:libpinyin/models/model14.text.tar.gz',
                        'ref': '185f0f175a90bcfc55cf3cf6ceff8d447a6269492c0ca1a1fc0748ea2c181363',
                        'directory': 'data',
                        'base-dir': ''
                    })

                # Enable tests in the libosinfo build, otherwise it leaves HAVE_CURL
                # undefined and that breaks with newer autotools
                elif module.name == 'libosinfo':
                    if 'variables' in element:
                        variables = element['variables']
                        if 'conf-extra' in variables:
                            variables['conf-extra'] = variables['conf-extra'] \
                                .replace('--enable-tests=no', '--enable-tests').rstrip()

                # librsvg is rusty, as such it needs some heavy special casing to
                # build with the vendored crates
                elif module.name == 'librsvg':

                    # Build depend on crates.bst
                    element['depends'].append({
                        'filename': 'base/crates.bst',
                        'type': 'build'
                    })

                    # Prepend some things to configure-commands
                    element_config = get_dict(element, 'config')
                    element_config['configure-commands'] = {
                        '(<)': [

                            # Copy the vendor directory into the source tree, because:
                            #   https://github.com/nikomatsakis/lalrpop/issues/272
                            'cp -a "%{datadir}/crates" "%{build-root}/cargo-crates"',

                            # Remove Cargo.toml, we're building from git and I think we
                            # prefer to always try to build against the latest things which
                            # have been added to the vendored crates
                            'rm rust/Cargo.lock || true'
                        ]
                    }
                elif module.name == 'upower':

                    # This module was a bit touchy when using a tarball
                    #
                    element_config = get_dict(element, 'config')
                    element_config['configure-commands'] = [
                        'if [ ! -x autogen.sh ]; then\n' + \
                        '  configure="./configure"\n'
                        'else\n' + \
                        '  configure="./autogen.sh"\n' + \
                        'fi\n' + \
                        '${configure} %{conf-args}'
                    ]

                # When running ./autogen.sh with NOCONFIGURE, we dont get
                # the implied maintainer mode option normally used with the "mm" C++ family
                # of libs
                elif module.name in ['atkmm', 'atkmm-1.6',
                                     'glibmm', 'glibmm-2.4',
                                     'gtkmm', 'gtkmm-3',
                                     'pangomm', 'pangomm-1.4']:
                    element_vars = get_dict(element, 'variables')
                    element_vars['conf-local'] = '--enable-maintainer-mode'

                element_file = os.path.join(config.directory, 'elements', element_name(module))
                bst_dump(element, element_file)

            # Now generate the stacks
            for stack_name in stacks:
                print "Generating {}.bst stack".format(stack_name)
                element = {}
                element['kind'] = 'stack'
                element['depends'] = sorted(stacks[stack_name])
                element_file = os.path.join(config.directory, 'elements', stack_name + '.bst')
                bst_dump(element, element_file)

        module_set = jhbuild.moduleset.load(config)
        module_list = module_set.get_module_list(args or config.modules,
                                                 config.skip, tags=config.tags,
                                                 include_suggests=not config.ignore_suggests)

        convert_project(config, module_list)
        convert_elements(config, module_set, module_list)

        return 0

register_command(cmd_bst)
